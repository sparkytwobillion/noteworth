Welcome To The Wonderful World Of Restaurants!
==============================================

####Description of the Problem and solution

Create a web application that allows users to do the following:
   * View a list of restaurants
   * Sort the list of restaurants by rating
   * View the details for a particular restaurant
   * Edit an existing restaurant
   * Add a new restaurant
   * Delete an existing restaurant
   * Add a rating to a specific restaurant

The solution chosen was to create a Rest Backend with a React front end. 

####Solution Focus

The primary focus of my solution is the backend. While I can use React, I would not 
consider myself to be a reasonable full stack developer. However, with access to a
knowledgeable React developer of which I could ask questions, I believe I could be
reasonable within a short time frame.

####Rationale Behind the Solution

After a reading of the code challenge I was fairly comfortable in presuming that 
the primary stack at Noteworth would likely include React which often likes to 
communicate with a restful or JSON api.

For me the risk was my lack of fluency with React. The backend solution was completed
within the time frame set out in the code challenge. However, due to my lack of experience
with React there were a few things that ate up some time in building the simple front end.

Despite the challenges I faced I feel like the backend code is a fair example of
my coding style and approach to problem solving.

####Tradeoffs, Missing Features, and other challenges

* Sorting on rating. Because of my approach in handling ratings I decided that the sorting
should take place on the frontend, given the fact that this solution does NOT implement pagination.
Due to running too far over time building the simple front end I left that out.

* Images for restaurants. Again, my lack of fluency with React. The front end that I built
does not allow for image uploads. I simply was spending too much time on this. I decided to 
punt on image uploads for my front implementation simply based on time. Be that as it may, by accessing
the Django Rest API interface at localhost:8000 image uploads do work. 

* All users are anonymous. A login feature was not part of the original requirements
so I skipped it and set this up to be all anonymous all the time. You may notice some _very odd_
trade offs in the backend such as disabling all CSRF and adding CORS headers to all requests. This 
is basically a quick workaround I chose for development. I would not choose to do this if I had more time.

* If I had more time, I would have had user creation and a login process so that the
application can restrict deletion, editing and what not to the user that created the entry.

* If it had been allowable I would have contacted a more knowledgeable colleague to help with my react
issues. In real life, reaching out and knowing when to ask for help is important, however, doing that 
in this situation would be cheating on a test. :)

* I would have liked to included unit tests for the React front end. But again, my lack of fluency.
That was a risk I chose to take on this task.

* I tried my best to show working times so that you could get a reasonable idea of how much time 
I actually spent on this task. If you examine the git log you will see commits made especially to demarcate
start and stop times of work sessions. These are not 100% accurate but are reasonably close.

[Daniel sparky Avila's LinkedIn](https://www.linkedin.com/in/sparkypdx/)

####Running the Code

It is presumed that the user will be using a virtual environment. 

After cloning the repository navigate to the top level directory that contains
the `requirments` directory.

Run the command
`pip install -r requirments/development.txt`

Navigate to the `frontend` directory NOTE: this code as built with node version v14.2.0
Run the command `yarn install`

You can run the React development server by running `yarn run start`

You can start the Django server by running `python manage.py runserver`

Both will need to be running to get the full enjoyment of this project.