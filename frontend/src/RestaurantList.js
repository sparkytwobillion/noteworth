import React from 'react';
import Container from 'react-bootstrap/Container';
import RestaurantListing from "./RestaurantListing";


class RestaurantList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            restaurants: null
        };
    }

    componentDidMount() {
        this.updateRestaurants()
    }

    updateRestaurants() {
        fetch('http://localhost:8000/', {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            response.json().then(data => {
                this.setState(prevState => ({restaurants: data, isLoaded: true}))
            });

        })
    }

    render() {
        const {isLoaded, restaurants} = this.state;

        if (isLoaded === false) {
            return <h3>...Loading</h3>
        } else {
            return (
                <Container>
                    {
                        restaurants.map((restaurant) => (

                            <RestaurantListing {...restaurant} />
                        ))
                    }
                </Container>
            )


        }
    }
}

export default RestaurantList