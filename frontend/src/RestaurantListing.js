import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from "react-bootstrap/Button";

import {LinkContainer} from "react-router-bootstrap";

class RestaurantListing extends React.Component {

    render() {
        const detailsPath = "/details/" + this.props.id;
        console.log(this.props)
        return (
            <Card style={{width: '100%'}}>
                <Card.Img variant="top" src={this.props.image}/>
                <Card.Body>
                    <Card.Title>{this.props.name}</Card.Title>
                    <Card.Text>
                        Cuisine: {this.props.cuisine.name}
                    </Card.Text>
                    <Card.Text>
                        Region: {this.props.cuisine.region}
                    </Card.Text>
                    <Card.Text>
                        City: {this.props.address.city}
                    </Card.Text>
                    <Card.Text>
                        Country: {this.props.address.country}
                    </Card.Text>
                    <LinkContainer to={detailsPath}>
                        <Button>Details</Button>
                    </LinkContainer>

                </Card.Body>
            </Card>


        )
    }
}

export default RestaurantListing