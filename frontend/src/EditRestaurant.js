import React from 'react';
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

class EditRestaurant extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: props.name || '',
            street: props.street || '',
            city: props.city || '',
            postalCode: props.postalCode || '',
            country: props.country || '',
            cuisine_name: props.cuisine_name || '',
            cuisine_region: props.cuisine_region || '',
            description: props.description || '',
            image: props.image || '',
            isLoaded: false,
            restaurantID: props.match.params.id
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
    }
    componentDidMount() {
        this.getRestaurantDetail()
    }
    getRestaurantDetail() {
        fetch('http://localhost:8000/' + this.state.restaurantID + '/', {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            response.json().then(data => {
                console.log(data)
                this.setState(prevState => ({
                    name: data.name,
                    street: data.address.street,
                    city: data.address.city,
                    state: data.address.state,
                    postalCode: data.address.postal_code,
                    country: data.address.country,
                    cuisine_name: data.cuisine.name,
                    cuisine_region: data.cuisine.region,
                    description: data.description,
                    image: data.image,
                    isLoaded: true
                }))
            });

        })
    }
    handleFormSubmit(e) {
        e.preventDefault();
        console.log(this.state.image)
        let restrauntData = {
            id: this.state.restaurantID,
            name: this.state.name,
            description: this.state.description,
            address: {
                street: this.state.street,
                city: this.state.city,
                state: this.state.state,
                country: this.state.country,
                postal_code: this.state.postal_code
            },
            cuisine: {
                name: this.state.cuisine_name,
                region: this.state.cuisine_region
            }

        }

        fetch('http://localhost:8000/' + this.state.restaurantID + '/',{
            method: "PUT",
            body: JSON.stringify(restrauntData),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            window.location = '/'
            })

    }

    handleChange(e) {
        let propName = e.target.name;
        let value = e.target.value


        this.setState( prevState => ({ [propName] : value}))
    }
    render() {

        let isLoaded = this.state.isLoaded
        if (isLoaded === false) {
            return <h3>...Loading</h3>
        } else {
        return (
            <Container>
                <Form>
                    <Form.Group controlId="restaurantName">
                        <Form.Label>Restaurant Name</Form.Label>

                        <Form.Control type="text" placeholder="Name of Restaurant"
                                      value={this.state.name} name="name" onChange={this.handleChange} />
                        <Form.Label>Street Address</Form.Label>
                        <Form.Control type="text" placeholder="Street Address" value={this.state.street}
                                      onChange={this.handleChange} name="street"/>

                        <Form.Label>City</Form.Label>
                        <Form.Control type="text" placeholder="City"
                                      onChange={this.handleChange} value={this.state.city} name="city" />
                        <Form.Label>State/Province Abbreviation 2 Characters</Form.Label>
                        <Form.Control type="text" onChange={this.handleChange}
                                      value={this.state.state} name="state" maxLength="2" />
                        <Form.Label>Postal Code</Form.Label>
                        <Form.Control type="text" placeholder="Postal Code" onChange={this.handleChange}
                                      value={this.state.postalCode} name="postal_code" />
                        <Form.Label>Country Abbreviation 3 Characters</Form.Label>
                        <Form.Control type="text" onChange={this.handleChange}
                                      value={this.state.country} name="country" maxLength="3"/>
                        <Form.Label>Cuisine Name</Form.Label>
                        <Form.Control type="text" placeholder="Cuisine Name" onChange={this.handleChange}
                                      value={this.state.cuisine_name} name="cuisine_name" />
                        <Form.Label>Cuisine Region</Form.Label>
                        <Form.Control type="text" placeholder="Cuisine Region" onChange={this.handleChange}
                                      value={this.state.cuisine_region} name="cuisine_region" />
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="textarea" placeholder="Description" onChange={this.handleChange}
                                      value={this.state.description} name="description" />

                    </Form.Group>
                    <Button variant="primary" type="submit" onClick={this.handleFormSubmit} onChange={this.handleChange}>
                        Edit
                    </Button>
                </Form>
            </Container>


        )}
    }
}

export default EditRestaurant