import React from 'react';
import { MemoryRouter, Switch, Route } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import { LinkContainer } from 'react-router-bootstrap';

import './App.css';
import NewRestaurant from "./NewRestaurant";
import RestaurantList from './RestaurantList'
import RestaurantDetails from "./Details";
import EditRestaurant from "./EditRestaurant";

const Home = RestaurantList;

const Add = NewRestaurant;


const App = () => (

  <MemoryRouter>
    <Container className="p-3">

        <h2>
          <ButtonToolbar className="custom-btn-toolbar">
            <LinkContainer to="/">
              <Button>List Restaurants</Button>
            </LinkContainer>
            <LinkContainer to="/add">
              <Button>Add Restaurant</Button>
            </LinkContainer>

          </ButtonToolbar>
        </h2>
                  <Switch>
            <Route path="/add">
              <Add />
            </Route>
            <Route path="/details/:id" component={RestaurantDetails} />
            <Route path="/edit/:id" component={EditRestaurant} />
            <Route path="/">
              <Home />
            </Route>
          </Switch>
    </Container>
  </MemoryRouter>
);

export default App;
