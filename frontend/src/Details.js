import React from 'react';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form"

class RestaurantDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurantID: props.match.params.id,
            restaurant: null,
            isLoaded: false
        };
        this.handleDelete = this.handleDelete.bind(this)
        this.handleOnChange = this.handleOnChange.bind(this)
        this.handleEdit = this.handleEdit.bind(this)
    }

    componentDidMount() {
        this.getRestaurantDetail()
    }

    handleEdit() {
        this.props.history.push('/edit/' + this.state.restaurantID)
    }
    handleDelete() {
        fetch('http://localhost:8000/' + this.state.restaurantID + '/', {
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            window.location = '/'

        })
    }
    handleOnChange(e){
        let value = e.target.value
        let payload = {restaurant: this.state.restaurantID, rating: value}
        fetch('http://localhost:8000/rating/ratings/', {
            method: "POST",
            body: JSON.stringify(payload),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            window.location = '/'
        })
    }

    getRestaurantDetail() {
        fetch('http://localhost:8000/' + this.state.restaurantID + '/', {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            response.json().then(data => {
                this.setState(prevState => ({restaurant: data, isLoaded: true}))
            });

        })
    }

    render() {
        let isLoaded = this.state.isLoaded
        if (isLoaded === false) {
            return <h3>...Loading</h3>
        } else {
            return (
                <Card style={{width: '100%'}}>
                    <Card.Img variant="top" src={this.state.restaurant.image}/>
                    <Card.Body>
                        <Card.Title>{this.state.restaurant.name}</Card.Title>
                        <Card.Text>
                            Cuisine: {this.state.restaurant.cuisine.name}
                        </Card.Text>
                        <Card.Text>
                            Region: {this.state.restaurant.cuisine.region}
                        </Card.Text>
                        <Card.Text>
                            City: {this.state.restaurant.address.city}
                        </Card.Text>
                        <Card.Text>
                            Country: {this.state.restaurant.address.country}
                        </Card.Text>
                        <Card.Text>
                            Rating: {this.state.restaurant.rating}
                        </Card.Text>
                        <Card.Text>
                            description: {this.state.restaurant.description}
                        </Card.Text>
                        <Card.Footer>
                            <Button variant="danger" onClick={this.handleDelete}>Delete</Button>
                            <Button onClick={this.handleEdit}>Edit Restaurant</Button>
                             <br />
                            <Form.Label>
                                Rate This Restaurant
                            </Form.Label>

                            <Form.Control as="select" custom onChange={this.handleOnChange}>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </Form.Control>
                        </Card.Footer>
                    </Card.Body>
                </Card>
            )
        }
    }
}

export default RestaurantDetails