import django.urls
from django.urls import path

from ratings.views.restaurant_viewset import restaurant_router
from ratings.views.rating_viewset import rating_router
urlpatterns = [
    path('',
         django.urls.include(restaurant_router.urls)),
    path('rating/',
         django.urls.include(rating_router.urls), name="ratings"),

]
