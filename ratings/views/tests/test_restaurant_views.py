from django.test import TestCase
from rest_framework.test import APIRequestFactory

import ratings.models
from ratings import factories
from ratings.views.restaurant_viewset import RestaurantViewSet


class TestRestaurantVeiws(TestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()

    def test_create_rest(self):
        """Can we create a restaurant?"""
        view = RestaurantViewSet.as_view(actions={'post': 'create'})
        address = {'street': 'street address', 'city': 'Some City', 'state':
            'OR', 'country': 'US', 'postal_code': '97068'}
        cuisine = {'name': 'Spanish', 'region': 'Spain'}
        payload = {'name': 'Fancy Feast', 'address': address,
                   'cuisine': cuisine, 'description': 'foo'}
        request = self.factory.post('/foo/', data=payload, format='json')
        outcome = view(request)
        self.assertEqual(201, outcome.status_code)
        queryset = ratings.models.Restaurant.objects.all()
        self.assertEqual(1, queryset.count())
        # Make sure the cuisine got made
        self.assertEqual(cuisine['region'], queryset[0].cuisine.region)
        # Make sure the address got made
        self.assertEqual(address['city'], queryset[0].address.city)

    def test_list(self):
        """Can we list multiple restaurants?"""
        view = RestaurantViewSet.as_view(actions={'get': 'list'})
        address = factories.AddressFactory(**{'street': 'street address',
                                              'city': 'Some City',
                                              'state': 'OR',
                                              'country': 'US',
                                              'postal_code': '97068'})
        cuisine = factories.CuisineFactory(**{'name': 'Spanish',
                                              'region': 'Spain'})
        factories.RestaurantFactory(**{'name': 'Fancy Feast',
                                       'address': address,
                                       'cuisine': cuisine})
        factories.RestaurantFactory(**{'name': 'Fancy Feast 2',
                                       'address': address,
                                       'cuisine': cuisine})

        request = self.factory.get('/foo/')
        outcome = view(request)
        self.assertEqual(200, outcome.status_code)
        self.assertEqual(2, len(outcome.data))

    def test_edit(self):
        """Can we edit an existing restaurant?"""
        view = RestaurantViewSet.as_view(actions={'patch': 'update'})
        address = factories.AddressFactory(**{'street': 'street address',
                                              'city': 'Some City',
                                              'state': 'OR',
                                              'country': 'US',
                                              'postal_code': '97068'})
        cuisine = factories.CuisineFactory(**{'name': 'Spanish',
                                              'region': 'Spain'})
        restaurant = factories.RestaurantFactory(**{'name': 'Fancy Feast',
                                                    'address': address,
                                                    'cuisine': cuisine})
        address = {'street': 'New Street', 'city': 'Some City', 'state':
            'OR', 'country': 'US', 'postal_code': '97068'}
        cuisine = {'name': 'Danish', 'region': 'Denmark'}
        payload = {'name': 'Danish House', 'address': address,
                   'cuisine': cuisine, 'description': 'a description'}
        request = self.factory.patch('/foo/', data=payload, format='json')
        outcome = view(request, pk=restaurant.id)
        self.assertEqual(address['street'], outcome.data['address'][
            'street'])

        self.assertEqual(cuisine['name'], outcome.data['cuisine']['name'])
        self.assertEqual(payload['name'], outcome.data['name'])

    def test_delete(self):
        """Can we delete a restaurant?"""
        view = RestaurantViewSet.as_view(actions={'delete': 'destroy'})
        address = factories.AddressFactory(**{'street': 'street address',
                                              'city': 'Some City',
                                              'state': 'OR',
                                              'country': 'US',
                                              'postal_code': '97068'})
        cuisine = factories.CuisineFactory(**{'name': 'Spanish',
                                              'region': 'Spain'})
        restaurant = factories.RestaurantFactory(**{'name': 'Fancy Feast',
                                                    'address': address,
                                                    'cuisine': cuisine})
        request = self.factory.delete('/foo/{}/'.format(restaurant.id))

        self.assertEqual(1, ratings.models.Restaurant.objects.all().count())
        outcome = view(request, pk=restaurant.id)
        self.assertEqual(204, outcome.status_code)
        self.assertEqual(0, ratings.models.Restaurant.objects.all().count())

