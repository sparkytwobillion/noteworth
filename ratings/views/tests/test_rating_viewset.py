import django.urls
from django.test import TestCase
from rest_framework.test import APIRequestFactory

import ratings.models
from ratings import factories
from ratings.views.rating_viewset import RatingViewSet


class TestRestaurantVeiws(TestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.address_data = {
            'street': '1313 Mockingbird Lane',
            'city': 'Los Angeles',
            'state': 'CA',
            'country': 'US',
            'postal_code': '97121'
        }
        self.address = factories.AddressFactory(**self.address_data)
        self.cuisine_data = {'name': 'Spanish', 'region': 'Spain'}
        cuisine = factories.CuisineFactory(**self.cuisine_data)
        self.restaurant = factories.RestaurantFactory(address=self.address,
                                                      cuisine=cuisine)

    def test_create_rating(self):
        """Can we create a rating for a restaurant?"""

        view = RatingViewSet.as_view(actions={'post': 'create'})
        #target = django.urls.reverse('ratings', kwargs={'pk': 1})
        payload = {'restaurant': self.restaurant.id, 'rating': 3}
        request = self.factory.post('/foo/', data=payload, format='json')
        outcome = view(request)
        self.assertEqual(201, outcome.status_code)
        target = ratings.models.Restaurant.objects.get(id=self.restaurant.id)
        self.assertEqual(3, target.rating)
        self.assertEqual(1, ratings.models.Rating.objects.all().count())

    def test_get_disabled(self):
        """We should not be able to call the get method"""
        view = RatingViewSet.as_view(actions={'post': 'create'})

        request = self.factory.get('/foo/')
        outcome = view(request)
        self.assertEqual(405, outcome.status_code)
