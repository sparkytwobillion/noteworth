import rest_framework.routers
from rest_framework import viewsets
from ratings import models
from ratings.serializers.restaurant import RestaurantSerializer

from rest_framework.authentication import (SessionAuthentication,
                                           BasicAuthentication)


class CsrfExempt(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class RestaurantViewSet(viewsets.ModelViewSet):

    serializer_class = RestaurantSerializer
    queryset = models.Restaurant.objects.all()
    authentication_classes = (CsrfExempt, BasicAuthentication)

    def create(self, request, *args, **kwargs):
        response = super(RestaurantViewSet, self).create(request, *args,
                                                         **kwargs)
        return response


restaurant_router = rest_framework.routers.SimpleRouter()
restaurant_router.register(
    '', RestaurantViewSet, basename='restaurants')
