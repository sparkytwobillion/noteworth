import rest_framework.authentication
import rest_framework.permissions
from rest_framework import viewsets, routers

import ratings.models
import ratings.views.restaurant_viewset
from ratings.serializers.rating import RatingSerializer


class RatingViewSet(viewsets.ModelViewSet):

    serializer_class = RatingSerializer
    queryset = ratings.models.Rating.objects.all()
    authentication_classes = (ratings.views.restaurant_viewset.CsrfExempt,
                              rest_framework.authentication.BasicAuthentication)

    def post(self, request, *args, **kwargs):
        return super(RatingViewSet, self).create(request, *args, **kwargs)


rating_router = routers.SimpleRouter()
rating_router.register(
        '', RatingViewSet, basename='ratings')
