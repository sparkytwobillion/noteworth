"""The Cuisine Serializer"""
from rest_framework import serializers

from ratings import models


class CuisineSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Cuisine
        fields = ['name', 'region']