"""The restaurant serializer"""
from rest_framework import serializers

from ratings import models
from .address import AddressSerializer
from .cuisine import CuisineSerializer


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Restaurant
        fields = '__all__'

    rating = serializers.SerializerMethodField()
    address = AddressSerializer()
    cuisine = CuisineSerializer()

    def get_rating(self, instance):
        """Get the calculated rating from the restaurnt instance
            Args:
                instance (Restaurant): an instance of a restaurant

            Returns:
                int: The calculated restaurant rating
        """

        return int(instance.rating)

    def create(self, validated_data):
        """We have to override the create method since we are nesting
        serializers

        Args:
            validated_data (dict): A dictionary of our validated data

        Returns:
            instance: An instance of the Restaurant class
        """

        address, _ = models.Address.objects.get_or_create(
                **validated_data.pop('address'))
        cuisine, _ = models.Cuisine.objects.get_or_create(
                **validated_data.pop('cuisine'))
        return models.Restaurant.objects.create(**validated_data,
                                                address=address,
                                                cuisine=cuisine)

    def update(self, instance, validated_data):
        """We have to override the update method since we are nesting
        serializers

        Args:
            instance (Restaurant): The instance of the class we are dealing
            attempting to update.

            validated_data (dict): A dictionary of our validated data

        Returns:
            instance: An instance of the Restaurant class
        """

        address_data = validated_data.pop('address')
        cuisine_data = validated_data.pop('cuisine')
        instance.address.city = address_data.get('city',
                                                 instance.address.city)
        instance.address.state = address_data.get('state',
                                                  instance.address.state)
        instance.address.street = address_data.get('street',
                                                   instance.address.street)
        instance.address.country = address_data.get('country',
                                                    instance.address.country)
        instance.address.postal_code = address_data.get('postal_code',
                                                        instance.address.postal_code)
        instance.address.save()
        instance.cuisine.name = cuisine_data.get('name',
                                                 instance.cuisine.name)
        instance.cuisine.region = cuisine_data.get('region',
                                                   instance.cuisine.region)
        instance.cuisine.save()
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance