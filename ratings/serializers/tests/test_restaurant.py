"""Test for the Restaurant Serializer"""
import django.test

from ratings.serializers.restaurant import RestaurantSerializer
from ratings import factories


class TestRestaurantSerializer(django.test.TestCase):
    def setUp(self) -> None:
        self.address_data = {
            'street': '1313 Mockingbird Lane',
            'city': 'Los Angeles',
            'state': 'CA',
            'country': 'US',
            'postal_code': '97121'
        }
        self.address = factories.AddressFactory(**self.address_data)
        ratings = [1, 2, 3, 4, 5]
        self.cuisine_data = {'name': 'Spanish', 'region': 'Spain'}
        cuisine = factories.CuisineFactory(**self.cuisine_data)
        self.restaurant = factories.RestaurantFactory(address=self.address,
                                                      cuisine=cuisine)
        for r in ratings:
            factories.RatingFactory(restaurant=self.restaurant, rating=r)

    def test_restaurant_rating(self):
        """Do we include the rating in the serialization?"""

        serializer = RestaurantSerializer(instance=self.restaurant)
        outcome = serializer.data['rating']
        self.assertEqual(3, outcome)

    def test_restaurant_address(self):
        """Does the restaurant record contain the serialized address?"""
        serializer = RestaurantSerializer(instance=self.restaurant)
        outcome = serializer.data['address']
        self.assertDictEqual(self.address_data, outcome)

    def test_restaurant_cuisine(self):
        """Does the restaurant record contain the cuisine information?"""
        serializer = RestaurantSerializer(instance=self.restaurant)
        outcome = serializer.data['cuisine']
        self.assertDictEqual(self.cuisine_data, outcome)