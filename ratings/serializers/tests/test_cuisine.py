"""Tests for the cuisine serializer"""
import django.test

from ratings import factories
from ratings.serializers.cuisine import CuisineSerializer


class TestCuisineSerializer(django.test.TestCase):

    def setUp(self) -> None:
        self.data = {'name': 'Russian', 'region': 'Russia'}
        self.cuisine = factories.CuisineFactory(**self.data)

    def test_serialize(self):
        """Can we serialize and address?"""

        serializer = CuisineSerializer(instance=self.cuisine)
        self.assertDictEqual(self.data, serializer.data)