"""Tests for the Address Serializer"""
import django.test

from ratings.serializers.address import AddressSerializer
from ratings import factories


class TestAddressSerializer(django.test.TestCase):

    def setUp(self) -> None:
        self.address_data = {
            'street': '1313 Mockingbird Lane',
            'city': 'Los Angeles',
            'state': 'CA',
            'country': 'US',
            'postal_code': '97121'
        }
        self.address = factories.AddressFactory(**self.address_data)

    def test_serialize(self):
        """Can we serialize and address?"""

        serializer = AddressSerializer(instance=self.address)
        self.assertDictEqual(self.address_data, serializer.data)