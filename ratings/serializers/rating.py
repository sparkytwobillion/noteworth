"""Rating Serializer"""

from rest_framework import serializers

from ratings import models


class RatingSerializer(serializers.ModelSerializer):
    """We only need this class because we are creating ratings.

    We won't be using this to view the rating of a restaurant.
    """
    class Meta:
        model = models.Rating
        fields = '__all__'
