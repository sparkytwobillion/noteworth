"""The Address Serializer"""
from rest_framework import serializers

from ratings import models


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Address
        fields = ['street', 'city', 'state', 'country', 'postal_code']