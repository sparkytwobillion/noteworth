import django.test
from rest_framework.test import APIRequestFactory

import ratings.factories.address
from ratings.models import Restaurant
from ratings.factories import cuisine
from ratings.factories import restaurant
from ratings.filters.restaurant import RestaurantFilter

class TestRestuarntFilter(django.test.TestCase):

    def setUp(self) -> None:
        self.request_factory = APIRequestFactory()

    def test_filter_for_cuisine(self):
        """Can we filter on the cuisine?"""
        my_cuisine = cuisine.CuisineFactory()
        expected_result = restaurant.RestaurantFactory(cuisine=my_cuisine)
        restaurant.RestaurantFactory.create_batch(5)
        queryset = Restaurant.objects.all()
        request = self.request_factory.get('/foo/',
                                           data={'cuisine': str(my_cuisine.id)})
        filter = RestaurantFilter()
        result = filter.filter_queryset(request, queryset, None)

        self.assertEqual(1, result.count())
        self.assertEqual(expected_result.id, result[0].id)

    def test_filter_for_city(self):
        """Can we filter on the city?"""
        my_cuisine = cuisine.CuisineFactory()
        address = ratings.factories.address.AddressFactory(city="Austin")
        expected_result = restaurant.RestaurantFactory(cuisine=my_cuisine,
                                                       address=address)
        restaurant.RestaurantFactory.create_batch(5)
        queryset = Restaurant.objects.all()
        request = self.request_factory.get('/foo/',
                                           data={
                                               'city': 'austin'})
        filter = RestaurantFilter()
        result = filter.filter_queryset(request, queryset, None)

        self.assertEqual(1, result.count())
        self.assertEqual(expected_result.id, result[0].id)

    def test_filter_for_country(self):
        """Can we filter on the country?"""
        my_cuisine = cuisine.CuisineFactory()
        address = ratings.factories.address.AddressFactory(city="Austin",
                                                           country="US")
        expected_result = restaurant.RestaurantFactory(cuisine=my_cuisine,
                                                       address=address)
        restaurant.RestaurantFactory.create_batch(5)
        queryset = Restaurant.objects.all()
        request = self.request_factory.get('/foo/',
                                           data={
                                               'country': 'us'})
        filter = RestaurantFilter()
        result = filter.filter_queryset(request, queryset, None)

        self.assertEqual(1, result.count())
        self.assertEqual(expected_result.id, result[0].id)

    def test_filter_for_all(self):
        """Can we get back a result if we use all params?"""

        my_cuisine = cuisine.CuisineFactory()
        address = ratings.factories.address.AddressFactory(city="Austin",
                                                           country="US")
        expected_result = restaurant.RestaurantFactory(cuisine=my_cuisine,
                                                       address=address)
        restaurant.RestaurantFactory.create_batch(5)
        queryset = Restaurant.objects.all()
        data = {'country': 'us', 'city': 'auStin', 'cuisine': str(
                my_cuisine.id)}
        request = self.request_factory.get('/foo/', data=data)
        filter = RestaurantFilter()
        result = filter.filter_queryset(request, queryset, None)

        self.assertEqual(1, result.count())
        self.assertEqual(expected_result.id, result[0].id)
