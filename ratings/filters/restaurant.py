from rest_framework import filters


class RestaurantFilter(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        """Filters the restaurants on any or none of city, country, cuisine

        Args:
            request (WSGIRequest): The standard Django request object. We will
            be looking for it's GET query values if any.

            queryset (QuerySet): The standard Django QuerySet containing all
            the Restaurants.

            view (View): The View we are using to access this filter.

        Returns:
            queryset: A QuerySet filtered by any or all of the GET query
            arguments
            passed in request.GET
        """

        country = request.GET.get('country')
        city = request.GET.get('city')
        cuisine = request.GET.get('cuisine')

        if country:
            queryset = queryset.filter(address__country__iexact=country)
        if city:
            queryset = queryset.filter(address__city__iexact=city)
        if cuisine:
            queryset = queryset.filter(cuisine_id=int(cuisine))

        return queryset

