import factory
from ratings.models import Cuisine


class CuisineFactory(factory.DjangoModelFactory):

    class Meta:
        model = Cuisine
