import factory

from ratings.models import Restaurant
from .address import AddressFactory
from .cuisine import CuisineFactory


class RestaurantFactory(factory.DjangoModelFactory):

    class Meta:
        model = Restaurant

    address = factory.SubFactory(AddressFactory)
    cuisine = factory.SubFactory(CuisineFactory)