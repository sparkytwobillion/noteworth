import factory

from ratings.models import Rating


class RatingFactory(factory.DjangoModelFactory):

    class Meta:
        model = Rating

