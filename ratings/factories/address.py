import factory
from ratings.models import Address


class AddressFactory(factory.DjangoModelFactory):

    class Meta:
        model = Address
