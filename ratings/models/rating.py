"""An rating model"""
from django.db import models
from django.utils.translation import gettext as _
from .base import BaseModel
from .restaurant import Restaurant


class Rating(BaseModel):

    rating = models.IntegerField(help_text=_("Your 1-5 rating of this "
                                             "resturant"))
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)