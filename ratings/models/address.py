"""An address model"""
from django.db import models
from django.utils.translation import gettext as _
from .base import BaseModel


class Address(BaseModel):

    street = models.TextField(max_length=256, help_text=_("Street Address of "
                                                          "the restaurant."),
                              blank=False)
    city = models.TextField(max_length=128, help_text=_("City where the "
                                                        "restaurnt is "
                                                        "located."))
    state = models.TextField(max_length=64, help_text=_("Province or State "
                                                        "if applicable"),
                             default="", blank=True)
    country = models.TextField(max_length=56, help_text=_("Country the "
                                                          "restaurant is "
                                                          "located in."))
    postal_code = models.TextField(max_length=10, help_text=_("Postal Code if "
                                                              "applicable."),
                                   default="", blank=True)

