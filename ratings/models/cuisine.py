"""A cuisine model"""
from django.db import models
from django.utils.translation import gettext as _
from .base import BaseModel


class Cuisine(BaseModel):

    name = models.TextField(max_length=64, help_text=_("Name of the Cuisine "
                                                       "Type."))
    region = models.TextField(max_length=64, help_text=_("Region if "
                                                         "applicable."),
                              blank=True, default='')