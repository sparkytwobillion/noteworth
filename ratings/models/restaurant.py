"""An restaurant model"""
from django.db import models
from django.utils.translation import gettext as _

from .address import Address
from .base import BaseModel
from .cuisine import Cuisine


class Restaurant(BaseModel):
    name = models.TextField(max_length=256, help_text=_("Name of the "
                                                        "restaurant."))
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    cuisine = models.ForeignKey(Cuisine, on_delete=models.PROTECT)
    description = models.TextField(max_length=256, help_text=_("Description "
                                                               "of "
                                                               "Restaurant"))
    image = models.ImageField(upload_to='images/',
                              help_text=_("Image of restaurant"),
                              blank=True, null=True)

    @property
    def rating(self):
        if self.rating_set.count() == 0:
            return 0
        return sum(self.rating_set.values_list('rating',
                                               flat=True)) / \
               self.rating_set.count()
