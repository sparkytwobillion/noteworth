"""Tests for the Cuisine Model"""
from django import test
from ..cuisine import Cuisine

class TestCuisineModel(test.TestCase):

    def test_new(self):
        """Can we even new up an address?"""
        kwargs = {'name': 'Mexican', 'region': 'Yucatan'}
        Cuisine.objects.create(**kwargs)