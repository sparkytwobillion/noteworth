"""Tests for the Address Model"""
from django import test

from ..address import Address


class TestAddressModel(test.TestCase):

    def test_new(self):
        """Can we even new up an address?"""
        kwargs = {'street': '1126 SW 12th', 'city': 'Portland',
        'state': 'OR', 'postal_code': '97203', 'country': 'US'}
        Address.objects.create(**kwargs)