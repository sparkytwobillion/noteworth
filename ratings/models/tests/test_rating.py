"""Tests for the Ratings Model"""
from django import test

from ..rating import Rating
from ratings.factories.restaurant import RestaurantFactory


class TestRatingModel(test.TestCase):

    def test_new(self):
        """Can we even new up an address?"""
        kwargs = {'rating': 1, 'restaurant': RestaurantFactory()}
        Rating.objects.create(**kwargs)