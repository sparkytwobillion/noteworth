"""Tests for the Restaurant Model"""
from django import test

from ratings.factories.rating import RatingFactory
from ratings.factories.restaurant import RestaurantFactory
from ratings.factories.cuisine import CuisineFactory
from ratings.factories.address import AddressFactory
from ..restaurant import Restaurant


class TestRestaurantModel(test.TestCase):

    def tearDown(self) -> None:
        Restaurant.objects.all().delete()

    def test_new(self):
        """Can we even new up a restaurant?"""
        kwargs = {'name': 'Fancy Pants', 'cuisine': CuisineFactory(),
                  'address': AddressFactory()}
        Restaurant.objects.create(**kwargs)

    def test_rating(self):
        """Can we get the restaurant's average rating?"""

        r = RestaurantFactory()
        ratings = [1, 2, 3, 4, 5]
        for rating in ratings:
            RatingFactory(rating=rating, restaurant=r)

        expected = 3

        # Code under test
        self.assertEqual(expected, r.rating)

    def test_no_ratings(self):
        """Do we return 0 if there are no ratings?"""

        r = RestaurantFactory()
        self.assertEqual(0, r.rating)
